package org.hspconsortium.platform.api.model;

public class ResetSandboxCommand implements Command {
    private DataSet dataSet;

    public DataSet getDataSet() {
        return dataSet;
    }
}
