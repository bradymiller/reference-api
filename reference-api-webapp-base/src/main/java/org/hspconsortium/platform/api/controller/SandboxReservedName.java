package org.hspconsortium.platform.api.controller;

public enum SandboxReservedName {
    hspc,
    hspc2,
    hspc3,
    hspc4,
    hspc5,
    test,
    sandbox,
    management,
    admin,
    reset,
    system
}
